﻿using System;
using System.Collections;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework.Media;

namespace BlackMagesAdventure
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        // GameScreen
        private int rows;
        private int columns;
        private const int TILESIZE = 30;
        private GameScreen gameScreen;

        // Hero
        private Hero vivi;

        // Enemies
        private List<Enemy> enemies;
        private int count = 0;

        // Chocobo
        private Chocobo chocobo;
        
        // Game info
        private SpriteFont gameInfoFont;

        // Levels
        private List<byte[,]> levels;
        private int currentLevel;

        // Game states
        private bool isGameOver;
        private bool isVictorious;

        // Background
        Texture2D background;

        // Sounds
        Song victorySound;
        Song gameOverSound;


        public Game1()
        {
            initGame();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            // Get the number of rows and columns from the level map array
            rows = levels[currentLevel].GetLength(0);
            columns = levels[currentLevel].GetLength(1);
            graphics.PreferredBackBufferHeight = rows * TILESIZE;
            graphics.PreferredBackBufferWidth = columns * TILESIZE;
            graphics.ApplyChanges();

            enemies = new List<Enemy>();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Texture2D[] tileTextures = { Content.Load<Texture2D>("sprites/tileSprite"), Content.Load<Texture2D>("sprites/earthSprite"), Content.Load<Texture2D>("sprites/waterSprite") };

            // TODO: use this.Content to load your game content here
            // Create hero
            vivi = new Hero(new Vector2(TILESIZE, GraphicsDevice.Viewport.Height - 3 * TILESIZE), spriteBatch, TILESIZE, Content);


            // Create enemies
            for (int x = 0; x < columns; x++)
            {
                for (int y = 0; y < rows; y++)
                {
                    if (levels[currentLevel][y, x] == 3)
                    {
                        enemies.Add(new Enemy(new Vector2(x * TILESIZE, y * TILESIZE), spriteBatch, TILESIZE, Content));
                        count++;
                    }
                }
            }

            // Create chocobo
            for (int x = 0; x < columns; x++)
            {
                for (int y = 0; y < rows; y++)
                {
                    if (levels[currentLevel][y, x] == 4)
                    {
                        chocobo = new Chocobo(new Vector2(x * TILESIZE, y * TILESIZE), spriteBatch, TILESIZE, Content);
                    }
                }
            }
         
            // Load game info font
            gameInfoFont = Content.Load<SpriteFont>("fonts/gameInfoFont");

            // Create the tiles map
            gameScreen = new GameScreen(spriteBatch, levels[currentLevel], TILESIZE, tileTextures);

            // Victory sound
            victorySound = Content.Load<Song>("sounds/victorySound");
            gameOverSound = Content.Load<Song>("sounds/gameOverSound");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        
        protected override void Update(GameTime gameTime)
        {
            // Only run the code if the game is not paused
            if (IsActive)
            {
                // Exit the game on ESC
                if (Keyboard.GetState().IsKeyDown(Keys.Escape)) Exit();

                // TODO: Add your update logic here
                // Update hero to check collision detections if the hero is alive and the game isn't over
                if (!isGameOver)
                {
                    if (vivi.IsAlive)
                    {
                        vivi.Update(gameTime, enemies, chocobo);
                        if (vivi.foundChocobo)
                        {

                            MediaPlayer.Play(victorySound);
                            if (currentLevel + 1 == levels.Count)
                            {
                                isVictorious = true;
                            } else
                            {
                                currentLevel++;
                                Initialize();
                            }
                        }
                    }
                    else
                    {
                        isGameOver = true;
                        MediaPlayer.Play(gameOverSound);
                    }

                    // Update enemies
                    foreach (Enemy enemy in enemies)
                    {
                        enemy.Update(gameTime);
                    }

                    // Update chocobo
                    chocobo.Update(gameTime);
                }
                // Update parent
                base.Update(gameTime);
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            // Clear the screen
            GraphicsDevice.Clear(Color.DarkOrange);

            // TODO: Add your drawing code here
            // Start all the drawings
            spriteBatch.Begin();

            // Draw the gameScreen
            gameScreen.Draw();

            // Draw the hero
            vivi.Draw();

            // Draw the enemies
            foreach (Enemy enemy in enemies)
            {
                enemy.Draw();
            }
            

            // Draw the Chocobo
            chocobo.Draw();

            // Draw the parent
            base.Draw(gameTime);

            // Write game info 
            WriteGameInfo();

            // Finish all the drawings
            spriteBatch.End();
        }

        private void WriteGameInfo()
        {
            //string heroPosition = string.Format("Vivi's position: ({0:0.0}, {1:0.0})", vivi.Position.X, vivi.Position.Y);
            string lifesRemaining = string.Format("Lifes left: {0}", vivi.AmountOfLifes);
            string curLevel = string.Format("Current level: {0}", currentLevel + 1);
            //string nrOfEnemies = string.Format("Enemies: {0}", count);

            //spriteBatch.DrawString(gameInfoFont, heroPosition, new Vector2(10, 0), Color.White);
            spriteBatch.DrawString(gameInfoFont, lifesRemaining, new Vector2(10, 10), Color.White);
            spriteBatch.DrawString(gameInfoFont, curLevel, new Vector2(200, 10), Color.White);
            if (isGameOver) spriteBatch.DrawString(gameInfoFont, "Game Over!", new Vector2(300, 200), Color.White, 0, new Vector2(0, 0), 5, SpriteEffects.None, 0);
            if (isVictorious) spriteBatch.DrawString(gameInfoFont, "Victory!", new Vector2(300, 200), Color.White, 0, new Vector2(0, 0), 5, SpriteEffects.None, 0);
            //spriteBatch.DrawString(gameInfoFont, nrOfEnemies, new Vector2(10, 10), Color.White);

        }

        /// <Legend>
        /// 0 = border tile
        /// 1 = ground
        /// 2 = water
        /// 3 = enemy
        /// 4 = chocobo, must be rescued = final target to win the game and proceed to the next level
        /// 5 = hero
        /// </Legend>
        private byte[,] level1 = new byte[,]
        {
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 3, 0, 0, 3, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 3, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        };

        private byte[,] level2 = new byte[,]
        {
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 3, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 3, 0, 3, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 5, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 3, 0, 0},
            {0, 1, 1, 1, 1, 2, 2, 1, 1, 1, 2, 2, 1, 1, 1, 2, 2, 1, 2, 2, 1, 1, 1, 1, 2, 2, 1, 1, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        };

        private void initGame()
        {
            graphics = new GraphicsDeviceManager(this);
            IsMouseVisible = true;
            Content.RootDirectory = "Content";
            isGameOver = false;
            isVictorious = false;
            levels = new List<byte[,]>();
            levels.Add(level1);
            levels.Add(level2);
            currentLevel = 0;
        }
    }
}
