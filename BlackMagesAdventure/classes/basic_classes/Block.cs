﻿using Microsoft.Xna.Framework;

namespace BlackMagesAdventure
{
    /// <summary>
    /// TOP level block class used as parent for all the custom classes
    /// Contains: 
    /// Vector2 position
    /// int height
    /// int width
    /// Rectangle Bounds
    /// </summary>
    class Block
    {
        private Vector2 position;
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        private int height;
        public int Height
        {
            get { return height; }
            set { height = value; }
        }

        private int width;
        public int Width
        {
            get { return width; }
            set { width = value; }
        }

        // Bounds used to detect collision
        public Rectangle Bounds
        {
            get { return new Rectangle((int) Position.X, (int) Position.Y, Width, Height); }
        }

        // Constructor
        public Block(Vector2 position, int size)
        {
            Position = position;
            Width = size;
            Height = size;
        }
    }
}
