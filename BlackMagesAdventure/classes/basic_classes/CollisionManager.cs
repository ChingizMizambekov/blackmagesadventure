﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackMagesAdventure
{
    class CollisionManager
    {
        public bool DetectCollisionBetweenTwoBoxObjects(Rectangle originalBox, Rectangle targetBox)
        {
            bool collisionDetected = false;
            if (originalBox.Intersects(targetBox)) collisionDetected = true;
            return collisionDetected;
        }


        /// <summary>
        ///  If hero collides with the enemy to the left or to the right of him => hero dies
        ///  If hero jumps and lands on the top of the enemy => enemy dies
        ///  To detect left, right, bottom collision, I have made 3 bars; LEFT and RIGHT: 1px wide and 28px high, respresenting LEFT and RIGHT border of the hero
        ///  The bottom bar is 1px high and 30px wide, representing the bottom border of the hero
        ///  Then I check whether one of the border bars intersects with the enemy bounds and detect which of them intersesct first
        /// </summary>
        public int HeroBeatsEnemy(Rectangle heroBox, Rectangle enemyBox)
        {
            int HeroBeatsEnemy = 0;

            Rectangle currentHeroLeftBorder = new Rectangle(heroBox.X, heroBox.Y + 1, 1, heroBox.Height - 2);
            Rectangle currentHeroRightBorder = new Rectangle(heroBox.X + heroBox.Width, heroBox.Y + 1, 1, heroBox.Height - 2);
            Rectangle currentHeroBottomBorder = new Rectangle(heroBox.X + 1, heroBox.Y + heroBox.Height, heroBox.Width, 1);
            currentHeroLeftBorder.Offset(-1, -1);
            currentHeroRightBorder.Offset(1, -1);
            currentHeroBottomBorder.Offset(0, 1);

            if (currentHeroBottomBorder.Intersects(enemyBox))
            {
                HeroBeatsEnemy = 1;
            }
            else if ((currentHeroLeftBorder.Intersects(enemyBox) || currentHeroRightBorder.Intersects(enemyBox)))
            {
                HeroBeatsEnemy = 2;
            }

            return HeroBeatsEnemy;
        }

    }
}
