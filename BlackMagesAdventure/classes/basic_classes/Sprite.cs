﻿using System;
using System.Collections;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BlackMagesAdventure
{
    class Sprite : Block
    {
        /// <summary>
        /// Sprite class extends the Block class
        /// Contains: 
        /// Texture2D Texture
        /// public SpriteBatch SpriteBatch
        /// Draw()
        /// </summary>
        public Texture2D Texture { get; set; }
        public SpriteBatch SpriteBatch { get; set; }

        public Sprite(Texture2D texture, Vector2 position, SpriteBatch batch, int size) : base(position, size)
        {
            Texture = texture;
            SpriteBatch = batch;
        }


        public virtual void Draw()
        {
            SpriteBatch.Draw(Texture, Position, Color.White);
        }
    }
}
