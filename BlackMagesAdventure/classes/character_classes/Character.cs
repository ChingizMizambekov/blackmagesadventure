﻿using System;
using System.Collections;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;

namespace BlackMagesAdventure
{
    class Character : Sprite
    {
        /// <summary>
        /// Character class extends the Sprite class
        /// Contains
        /// Vector2 Movement
        /// bool IsAlive
        /// void UpdatePositionBasedOnMovement()
        /// bool IsOnTheGround()
        /// </summary>
        public Vector2 Movement { get; set; }
        public CollisionManager collisionManager;
        private ContentManager contentManager;

        protected Song fireSound;
        protected Song dieSound;

        private bool isAlive;

        public bool IsAlive
        {
            get { return isAlive; }
            set { isAlive = value; }
        }


        public Character(Texture2D texture, Vector2 position, SpriteBatch batch, int size, ContentManager contManager) : base(texture, position, batch, size)
        {
            IsAlive = true;
            collisionManager = new CollisionManager();
            contentManager = contManager;
            fireSound = contentManager.Load<Song>("sounds/fireballSound");
            dieSound = contentManager.Load<Song>("sounds/dieSound");
        }

        protected void UpdatePositionBasedOnMovement(GameTime gametime)
        {
            //Position += Movement * (float)gametime.ElapsedGameTime.TotalMilliseconds / 15; // Sliding effect
            Position += Movement;
        }

        // Check whether the character stands on the ground
        public virtual bool IsOnTheGround()
        {
            Rectangle ground = Bounds;

            // Move the rectangle in the direction given in the X & Y parameters
            ground.Offset(0, 1);
            return !GameScreen.CurrentGameScreen.HasRoomForRectangle(ground);
        }

        public override void Draw()
        {
            if (IsAlive) base.Draw();
        }


    }
}
