﻿using System;
using System.Collections;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BlackMagesAdventure
{
    class Chocobo : Character
    {
        public Chocobo(Vector2 position, SpriteBatch batch, int size, ContentManager contManager) : base(contManager.Load<Texture2D>("sprites/chocoboSprite"), position, batch, size, contManager)
        {
            
        }

        public void Update(GameTime gameTime)
        {
        }

    }
}
