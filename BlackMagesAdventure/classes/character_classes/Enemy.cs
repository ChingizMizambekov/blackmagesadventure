﻿using System;
using System.Collections;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BlackMagesAdventure
{
    class Enemy : Character
    {
        private int count;
        private Random random;
        private ContentManager contentManager;
        private string[] movingState = { "MOVING_RIGHT", "MOVING_LEFT" };
        private int movingStateIndex;
        public Enemy(Vector2 position, SpriteBatch batch, int size, ContentManager contManager) : base(contManager.Load<Texture2D>("sprites/mage_rightSprite"), position, batch, size, contManager)
        {
            count = 0;
            // Make sure the random number is random
            random = new Random(Guid.NewGuid().GetHashCode());
            // Generate random speed between 1 and 0.1
            Movement = Vector2.UnitX * ((float) 1 / random.Next(1, 10));
            contentManager = contManager;
            movingStateIndex = 0;
        }

        public void Update(GameTime gameTime)
        {
            Move();
            AdjustSprite();
        }

        private void Move()
        {
            count += random.Next(1, 10);
            Position += Movement;
            
            if (count % 500 == 0 || !IsOnTheGround())
            {
                Movement = -Movement;
                movingStateIndex++;
                if (movingStateIndex == movingState.Length) movingStateIndex = 0;
                
            }


        }

        public override bool IsOnTheGround()
        {
            Rectangle groundLeft = Bounds;
            Rectangle groundRight = Bounds;

            // Check if the enemy doesnt fall off the ground
            groundLeft.Offset(-30, 1);
            groundRight.Offset(30, 1);
            return !GameScreen.CurrentGameScreen.HasRoomForRectangle(groundLeft) && !GameScreen.CurrentGameScreen.HasRoomForRectangle(groundRight);
        }

        private Rectangle BoundingBox()
        {
            return new Rectangle((int)Position.X, (int)Position.Y, Height, Width);
        }

        private void AdjustSprite()
        {
            switch (movingState[movingStateIndex])
            {
                case "MOVING_LEFT":
                    Texture = contentManager.Load<Texture2D>("sprites/mage_leftSprite");
                    break;
                case "MOVING_RIGHT":
                    Texture = contentManager.Load<Texture2D>("sprites/mage_rightSprite");
                    break;
                default:
                    Texture = contentManager.Load<Texture2D>("sprites/mage_rightSprite");
                    break;
            }
        }
        


    }
}
