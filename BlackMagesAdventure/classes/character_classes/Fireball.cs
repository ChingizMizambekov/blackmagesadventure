﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace BlackMagesAdventure
{
    class Fireball : Character
    {
        const int MAX_DISTANCE = 500;
        public bool IsVisible;
        public Vector2 StartPosition;
        public Vector2 Speed;
        public string Direction;
        public Fireball(Vector2 position, SpriteBatch batch, int size, string direction, ContentManager contManager) : base(contManager.Load<Texture2D>("sprites/fireballSprite"), position, batch, size, contManager)
        {
            Position = position;
            StartPosition = position;            
            IsVisible = true;
            collisionManager = new CollisionManager();
            Direction = direction;
            if (Direction == "RIGHT")
            {
                Speed = new Vector2(10, 0);
            } else if (Direction == "LEFT")
            {
                Speed = new Vector2(-10, 0);
            }
        }

        public void Fire()
        {
            Position += Speed;
            if (Vector2.Distance(StartPosition, Position) > MAX_DISTANCE)
            {
                IsVisible = false;
            }
        }

        public void Update(GameTime gameTime)
        {
            Fire();
        }

        public void Update(GameTime gameTime, List<Enemy> enemies)
        {
            Fire();
            DetectCollisionWithEnemies(enemies);
        }
        

        public override void Draw()
        {
            if (IsVisible) base.Draw();
        }
        

        public void DetectCollisionWithEnemies(List<Enemy> enemies)
        {
            var index = 0;
            while (index < enemies.Count)
            {
                if (IsVisible && collisionManager.HeroBeatsEnemy(Bounds, enemies[index].Bounds) == 1) // Collision detected, hero beats enemy
                {
                    enemies[index].IsAlive = false;
                    enemies.RemoveAt(index);
                    IsVisible = false;
                }
                else // No collision detected, proceed to the next enemy object
                {
                    index++;
                }
            }
        }
    }
}
