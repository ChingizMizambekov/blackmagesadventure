﻿using System;
using System.Collections;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;
using Microsoft.Xna.Framework.Media;

namespace BlackMagesAdventure
{
    class Hero : Character
    {
        //const int MAX_FIREBALLS = 20;
        private ContentManager contentManager;
        private Vector2 previousPosition;
        private Vector2 originalPosition;
        private string movingState;
        private string lastMovingDirection;
        private int counter = 0;
        public bool foundChocobo;

        private int amountOfLifes;
        public int AmountOfLifes
        {
            get { return amountOfLifes; }
            set { amountOfLifes = value; }
        }

        public List<Fireball> FireBalls = new List<Fireball>();
        


        public Hero(Vector2 position, SpriteBatch batch, int size, ContentManager contManager) : base(contManager.Load<Texture2D>("sprites/vivi_standSprite"), position, batch, size, contManager)
        {
            AmountOfLifes = 3;
            originalPosition = position;
            contentManager = contManager;
            movingState = "NONE";
            lastMovingDirection = "RIGHT";
            foundChocobo = false;

        }
        

        public void Update(GameTime gameTime, List<Enemy> enemies, Chocobo chocobo)
        {
            ListenToKeyboardAndUpdateMovement(gameTime);
            AffectWithGravity();
            SimulateFriction();
            MoveAsFarAsPossibleAndCalculateDirection(gameTime);
            DetectCollisions(enemies, chocobo);
            //if (fireball != null) fireball.Update(gameTime, enemies);
            foreach (Fireball fireball in FireBalls)
            {
                fireball.Update(gameTime, enemies);
            }
            AdjustSprite();
        }

        private void AdjustSprite()
        {
            switch (movingState)
            {
                case "MOVING_LEFT": Texture = contentManager.Load<Texture2D>("sprites/vivi_leftSprite");
                    break;
                case "MOVING_RIGHT":
                    Texture = contentManager.Load<Texture2D>("sprites/vivi_rightSprite");
                    break;
                case "NONE":
                    Texture = contentManager.Load<Texture2D>("sprites/vivi_standSprite");
                    break;
                default:
                    Texture = contentManager.Load<Texture2D>("sprites/vivi_standSprite");
                    break;
            }
        }

        private void ListenToKeyboardAndUpdateMovement(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();
            // Move left: arrow LEFT
            if (keyboardState.IsKeyDown(Keys.Left)) Movement += new Vector2(-1, 0); 
            // Move right: arrow RIGHT
            if (keyboardState.IsKeyDown(Keys.Right)) Movement += new Vector2(1, 0); 
            // Jump only of the hero stands on the ground: arrow UP or SPACE
            if (keyboardState.IsKeyDown(Keys.Up) && IsOnTheGround()) Movement = -Vector2.UnitY * 30; 
            // Shoot fireballs with SPACE key
            if (keyboardState.IsKeyDown(Keys.Space))
            {
                // Otherwise event fires up to 4-5 times until the key is back in up state
                if (counter % 5 == 0)
                {
                    FireBalls.Add(new Fireball(new Vector2(Position.X, Position.Y + Texture.Height / 5), SpriteBatch, 15, lastMovingDirection, contentManager));
                    MediaPlayer.Play(fireSound);
                }
                counter++;

            }
            
        }

        

        private void MoveAsFarAsPossibleAndCalculateDirection(GameTime gameTime)
        {
            previousPosition = Position;
            base.UpdatePositionBasedOnMovement(gameTime);
            Position = GameScreen.CurrentGameScreen.WhereCanIGetGo(previousPosition, Position, Bounds);
            Vector2 distance = previousPosition - Position;
            var distanceInX = distance.X;
            var distanceInY = distance.Y;

            if (distanceInX < 0 && distanceInY == 0)
            {
                movingState = "MOVING_RIGHT";
                lastMovingDirection = "RIGHT";
            } else if (distanceInX > 0 && distanceInY == 0)
            {
                movingState = "MOVING_LEFT";
                lastMovingDirection = "LEFT";
            } else if (distanceInX == 0 && distanceInY == 0)
            {
                movingState = "NONE";
            } else if (distanceInY != 0)
            {
                movingState = "JUMPING";
            }
            //Trace.WriteLine("X: " + distanceInX);
            //Trace.WriteLine("Y: " + distanceInY);
            //Trace.WriteLine("Moving state: " + movingState);
        }

        private void AffectWithGravity()
        {
            Movement += Vector2.UnitY * .95f;
        }

        private void SimulateFriction()
        {
            Movement -= Movement * new Vector2(.25f, .1f);
        }

        private void DetectCollisions(List<Enemy> enemies, Chocobo chocobo)
        {
            DetectCollisionWithWater();
            DetectCollisionWithEnemies(enemies);
            DetectCollisionWithChocobo(chocobo);
        }

        // Detect collision with enemies and remove them from the list if they are dead
        private void DetectCollisionWithEnemies(List<Enemy> enemies)
        {
            var index = -1;
            while (index < enemies.Count - 1)
            {
                index++;
                if (collisionManager.HeroBeatsEnemy(Bounds, enemies[index].Bounds) == 1) // Collision detected, hero beats enemy
                {
                    enemies[index].IsAlive = false;
                    enemies.RemoveAt(index);
                }
                else if (collisionManager.HeroBeatsEnemy(Bounds, enemies[index].Bounds) == 2) // Collision detected, enemy beats hero
                {
                    HeroDies();
                } 
            }
        }
        

        // Detect collision with water
        private void DetectCollisionWithWater()
        {
            foreach (Tile tile in GameScreen.CurrentGameScreen.Tiles)
            {
                if (tile.Type == 2)
                {
                    if (collisionManager.DetectCollisionBetweenTwoBoxObjects(Bounds, tile.Bounds))
                    {
                        HeroDies();
                    }
                }
            }
        }

        private bool DetectCollisionWithChocobo(Chocobo chocobo)
        {
            bool win = false;
            if (collisionManager.DetectCollisionBetweenTwoBoxObjects(Bounds, chocobo.Bounds))
            {
                win = HeroWins();
            }
            return win;
        }      


        private void HeroDies()
        {
            if (amountOfLifes - 1 > 0 && IsAlive)
            {
                amountOfLifes -= 1;
                Position = originalPosition;
                MediaPlayer.Play(dieSound);
            } else if (amountOfLifes - 1 == 0)
            {
                amountOfLifes -= 1;
                IsAlive = false;

            }
        }

        private bool HeroWins()
        {
            foundChocobo = true;
            //Trace.WriteLine("Victory!");
            return true;
        }


        public override void Draw()
        {
            base.Draw();
            //Trace.WriteLine(FireBalls.Count);
            foreach (Fireball fireball in FireBalls)
            {
                fireball.Draw();
            }
        }
    }
}
