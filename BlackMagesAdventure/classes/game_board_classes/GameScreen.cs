﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BlackMagesAdventure
{
    class GameScreen
    {

        // Collection of tiles
        public Tile[,] Tiles { get; set; }

        // Tile size
        public int TileSize;

        // Level containing the "map"
        public byte[,] Level;

        // number of columns of the gamescreen
        private int columns;

        public int Columns
        {
            get { return columns; }
            set { if (value >= 0) { columns = value; } }
        }

        // number of rows of the gamescreen
        private int rows;

        public int Rows
        {
            get { return rows; }
            set { if (value >= 0) { rows = value; } }
        }

        // Array containing all possible sprites used for the tiles        
        public Texture2D[] TileTextures { get; set; }
        public SpriteBatch SpriteBatch { get; set; }

        // Reference to the current game screen object that is being saved on the class. Accessible from anywhere
        public static GameScreen CurrentGameScreen { get; private set; }

        // Constructor
        public GameScreen(SpriteBatch batch, byte[,] level, int tileSize, Texture2D[] tileTextures)
        {
            SpriteBatch = batch;
            Level = level;
            Rows = Level.GetLength(0);
            Columns = Level.GetLength(1);
            TileSize = tileSize;
            TileTextures = tileTextures;

            createTiles();
            blockBorderTiles();

            GameScreen.CurrentGameScreen = this;
        }

        private void createTiles()
        {
            // Temp variables
            Texture2D tileTexture;
            bool tileBlocked;
            Vector2 tilePosition;
            int type;
            bool tileDrawable;

            // Initialize the Tiles array
            Tiles = new Tile[Columns, Rows];
            for (int x = 0; x < Columns; x++)
            {
                for (int y = 0; y < Rows; y++)
                {
                    // Check the level "map" to assign the correct tile textures to the corresponding tiles
                    switch (Level[y, x])
                    {
                        case 0:
                            tileTexture = TileTextures[0];
                            tileBlocked = false;
                            type = 0;
                            tileDrawable = true;
                            break;
                        case 1:
                            tileTexture = TileTextures[1];
                            tileBlocked = true;
                            type = 1;
                            tileDrawable = true;
                            break;
                        case 2:
                            tileTexture = TileTextures[2];
                            tileBlocked = false;
                            type = 2;
                            tileDrawable = true;
                            break;
                        default:
                            tileTexture = TileTextures[0];
                            tileBlocked = false;
                            type = 0;
                            tileDrawable = false;
                            break;
                    }
                    // Calculate the position of a tile and create it
                    tilePosition = new Vector2(x * TileSize, y * TileSize);
                    Tiles[x, y] = new Tile(tileTexture, tilePosition, SpriteBatch, TileSize, tileBlocked, type, tileDrawable);
                }
            }
        }

        // Block the border tiles
        private void blockBorderTiles()
        {
            for (int x = 0; x < Columns; x++)
            {
                for (int y = 0; y < Rows; y++)
                {
                    if (x == 0 || x == Columns - 1 || y == 0 || y == Rows - 1)
                    {
                        // Set border tiles to blocked and assign them Type of 10
                        Tiles[x, y].IsBlocked = true;
                        Tiles[x, y].Type = 10;
                    }
                }
            }
        }

        // Draw all tiles of the gamescreen
        public void Draw()
        {
            foreach (var tile in Tiles)
            {
                if (tile.Type != 3) tile.Draw();
            }
        }

        public bool HasRoomForRectangle(Rectangle rectangleToCheck)
        {
            foreach (var tile in Tiles)
            {
                if (tile.IsBlocked && tile.Bounds.Intersects(rectangleToCheck))
                {
                    return false;
                }
            }
            return true;
        }

        // Gebaseerd op code van xnafan.com
        public Vector2 WhereCanIGetGo(Vector2 originalPosition, Vector2 destination, Rectangle boundingRectangle)
        {
            // Calculate the movement from origin to destination
            Vector2 movementToTry = destination - originalPosition;
            // Assume that the destination is a non blocked area
            Vector2 furthestAvalaibleLocationSoFar = originalPosition;
            // Try approx once per half px, or at least 1 time for very small movements
            int numberOfStepsToBreakMovementInto = (int)(movementToTry.Length() * 2) + 1;
            // Figure out how far one step is
            Vector2 oneStep = movementToTry / numberOfStepsToBreakMovementInto;

            // Check one small step at a time
            for (int i = 1; i < numberOfStepsToBreakMovementInto; i++)
            {
                Vector2 positionToTry = originalPosition + oneStep * i;
                Rectangle newBoundary = CreateRectangleAtPosition(positionToTry, boundingRectangle.Width, boundingRectangle.Height);
                if (HasRoomForRectangle(newBoundary)) { furthestAvalaibleLocationSoFar = positionToTry; }
                else
                {
                    bool isDiagonalMove = movementToTry.X != 0 && movementToTry.Y != 0;
                    if (isDiagonalMove)
                    {
                        int stepsLeft = numberOfStepsToBreakMovementInto - (i - 1);

                        Vector2 remainingHorizontalMovement = oneStep.X * Vector2.UnitX * stepsLeft;
                        Vector2 finalPositionIfMovingHorizontally = furthestAvalaibleLocationSoFar + remainingHorizontalMovement;
                        furthestAvalaibleLocationSoFar = WhereCanIGetGo(furthestAvalaibleLocationSoFar, finalPositionIfMovingHorizontally, boundingRectangle);

                        Vector2 remainingVerticalMovement = oneStep.Y * Vector2.UnitY * stepsLeft;
                        Vector2 finalPositionIfMovingVertically = furthestAvalaibleLocationSoFar + remainingVerticalMovement;
                        furthestAvalaibleLocationSoFar = WhereCanIGetGo(furthestAvalaibleLocationSoFar, finalPositionIfMovingVertically, boundingRectangle);
                    }
                    break;
                }
            }
            return furthestAvalaibleLocationSoFar;
        }

        private Rectangle CreateRectangleAtPosition(Vector2 positionToTry, int width, int height)
        {
            return new Rectangle((int)positionToTry.X, (int)positionToTry.Y, width, height);
        }
    }
}
