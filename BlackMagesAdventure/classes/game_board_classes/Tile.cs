﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BlackMagesAdventure
{
    class Tile : Sprite
    {

        // Used for collision detection
        public bool IsBlocked { get; set; }
        public bool IsDrawable { get; set; }
        public int Type;

        // Constructor
        public Tile(Texture2D texture, Vector2 position, SpriteBatch batch, int size, bool isBlocked, int type, bool isDrawable) : base(texture, position, batch, size)
        {
            IsBlocked = isBlocked;
            IsDrawable = isDrawable;
            Type = type;
        }

        // Draw all the tiles static tiles, except the empty ones
        public override void Draw()
        {
            if ((IsBlocked || IsDrawable) && Type != 0) base.Draw(); 
        }
    }
}
